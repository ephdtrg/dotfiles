set nocompatible              " required
filetype off                  " required
set hidden
set showtabline=0
let g:ale_emit_conflict_warnings = 0
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
"-------------------=== Code/Project navigation ===-------------
Plugin 'vim-airline/vim-airline'            " Lean & mean status/tabline for vim
Plugin 'vim-airline/vim-airline-themes'     " Themes for airline
Plugin 'fisadev/FixedTaskList.vim'          " Pending tasks list
Plugin 'yuttie/comfortable-motion.vim'      " Smooth scrolling
Plugin 'thaerkh/vim-indentguides'           " Visual representation of indents
Plugin 'neomake/neomake'                    " Asynchronous Linting and Make Framework
Plugin 'Shougo/deoplete.nvim'               " Asynchronous Completion
Plugin 'roxma/nvim-yarp'                    " Deoplete Dependency #1
Plugin 'roxma/vim-hug-neovim-rpc'           " Deoplete Dependency #2
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
"-------------------=== Other ===-------------------------------
Plugin 'tpope/vim-surround'                 " Parentheses, brackets, quotes, XML tags, and more
Plugin 'jnurmine/Zenburn'                   " Colorscheme Zenburn
Plugin 'altercation/vim-colors-solarized'   " Colorscheme Solarized
Plugin 'dracula/vim'                        " Colorscheme Dracula
Plugin 'jreybert/vimagit'                   " Git Operations
Plugin 'chriskempson/base16-vim'            " Base 16 colors
Plugin 'ryanoasis/vim-devicons'             " Dev Icons
Plugin 'reedes/vim-pencil'
Plugin 'jceb/vim-orgmode'
Plugin 'tpope/vim-speeddating'

"-------------------=== Snippets support ===--------------------
Plugin 'garbas/vim-snipmate'                " Snippets manager
Plugin 'MarcWeber/vim-addon-mw-utils'       " dependencies #1
Plugin 'tomtom/tlib_vim'                    " dependencies #2
Plugin 'honza/vim-snippets'                 " snippets repo

"-------------------=== Languages support ===-------------------
Plugin 'scrooloose/nerdcommenter'           " Easy code documentation
Plugin 'mitsuhiko/vim-sparkup'              " Sparkup(XML/jinja/htlm-django/etc.) support
Plugin 'w0rp/ale'

"-------------------=== Python  ===-----------------------------
Plugin 'zchee/deoplete-jedi'
Plugin 'hynek/vim-python-pep8-indent'
Plugin 'mitsuhiko/vim-python-combined'
Plugin 'jmcantrell/vim-virtualenv'

"-------------------=== C/C++  ===------------------------------
Plugin 'zchee/deoplete-clang'

"-------------------=== Blockchain  ===-------------------------
Plugin 'tomlion/vim-solidity'
Plugin 'isRuslan/vim-es6'
Plugin 'steelsojka/deoplete-flow'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype on
filetype plugin on
filetype plugin indent on

"=====================================================
"" General settings
"=====================================================
if filereadable(expand("~/.vimrc_background"))
  source ~/.vimrc_background
endif
set encoding=utf-8
let base16colorspace=256
set t_Co=256                                " 256 colors
set guifont=Inconsolata\ For\ Powerline\ 14
"colorscheme zenburn                       " set vim colorscheme
"let g:airline_theme='zenburn'             " set airline theme
colorscheme eldar                       " set vim colorscheme
set background=dark
"let g:airline_theme='zenburn'             " set airline theme
let g:airline_theme='laederon'             " set airline theme
syntax enable                               " enable syntax highlighting

"set pyxversion=0
let g:python_host_prog  = '/home/euphemia/.pyenv/versions/neovim2/bin/python'
let g:python3_host_prog = '/home/euphemia/.pyenv/versions/neovim3/bin/python'
let g:loaded_python_provider = 1
set shell=/usr/bin/zsh
"hi x235_Grey15 ctermfg=235 guifg=#262626 
"hi x248_Grey66 ctermfg=248 guifg=#a8a8a8 
set number                                  " show line numbers
highlight LineNr cterm=NONE ctermfg=248 ctermbg=235
set ruler
set ttyfast                                 " terminal acceleration
set guicursor=n:blinkon1

set tabstop=4                               " 4 whitespaces for tabs visual presentation
set shiftwidth=4                            " shift lines by 4 spaces
set smarttab                                " set tabs for a shifttabs logic
set expandtab                               " expand tabs into spaces
set autoindent                              " indent when moving to the next line while writing code

set cursorline                              " shows line under the cursor's line
set showmatch                               " shows matching part of bracket pairs (), [], {}

set enc=utf-8	                            " utf-8 by default

set nobackup 	                            " no backup files
set nowritebackup                           " only in case you don't want a backup file while editing
set noswapfile 	                            " no swap files

set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?

set scrolloff=20                            " let 10 lines before/after cursor during scroll

set clipboard+=unnamedplus                   " use system clipboard

set exrc                                    " enable usage of additional .vimrc files from working directory
set secure                                  " prohibit .vimrc files to execute shell, create files, etc...

"=====================================================
"" Tabs / Buffers settings
"=====================================================
tab sball
set switchbuf=useopen
set laststatus=2
nmap <F9> :bprev<CR>
nmap <F10> :bnext<CR>
nmap <silent> <leader>q :SyntasticCheck # <CR> :bp <BAR> bd #<CR>

"=====================================================
"" Neomake Settings 
"=====================================================
call neomake#configure#automake('w')
let g:neomake_open_list = 2

"=====================================================
"" Deoplete  Settings 
"=====================================================
" Use deoplete.
let g:deoplete#enable_at_startup = 1
"=====================================================
"" NERDTree settings
"=====================================================
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '__pycache__$']     " Ignore files in NERDTree
let NERDTreeWinSize=40
autocmd VimEnter * if !argc() | NERDTree | endif  " Load NERDTree only if vim is run without arguments
nmap " :NERDTreeToggle<CR>
"=====================================================
"" Relative Numbering 
"=====================================================
nnoremap <F4> :set relativenumber!<CR>

"=====================================================
"" Search settings
"=====================================================
set incsearch	                            " incremental search
set hlsearch	                            " highlight search results

"=====================================================
"" Comfortable Motion Settings
"=====================================================
let g:comfortable_motion_scroll_down_key = "j"
let g:comfortable_motion_scroll_up_key = "k"
let g:comfortable_motion_scroll_down_key = "Up"
let g:comfortable_motion_scroll_up_key = "Down"
let g:comfortable_motion_no_default_key_mappings = 1
let g:comfortable_motion_impulse_multiplier = 25  " Feel free to increase/decrease this value.
nnoremap <silent> <C-d> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 2)<CR>
nnoremap <silent> <C-u> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -2)<CR>
nnoremap <silent> <C-f> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 4)<CR>
nnoremap <silent> <C-b> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -4)<CR>

"=====================================================
"" AirLine settings
"=====================================================
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#formatter='unique_tail'
let g:airline_powerline_fonts=1
"=====================================================
"" NERDComment Settings 
"=====================================================
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1


"=====================================================
"" DevIcon Settings
"=====================================================
" loading the plugin 
let g:webdevicons_enable = 1

" adding to vim-airline's tabline
let g:webdevicons_enable_airline_tabline = 1

" adding to vim-airline's statusline
let g:webdevicons_enable_airline_statusline = 1

" turn on/off file node glyph decorations (not particularly useful)
let g:WebDevIconsUnicodeDecorateFileNodes = 1

" use double-width(1) or single-width(0) glyphs 
" only manipulates padding, has no effect on terminal or set(guifont) font
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1

" the amount of space to use after the glyph character (default ' ')
let g:WebDevIconsNerdTreeAfterGlyphPadding = ' '

" change the default character when no match found
let g:WebDevIconsUnicodeDecorateFileNodesDefaultSymbol = 'ƛ'

" set a byte character marker (BOM) utf-8 symbol when retrieving file encoding
" disabled by default with no value
let g:WebDevIconsUnicodeByteOrderMarkerDefaultSymbol = ''

" enable folder/directory glyph flag (disabled by default with 0)
let g:WebDevIconsUnicodeDecorateFolderNodes = 1

" enable open and close folder/directory glyph flags (disabled by default with 0)
let g:DevIconsEnableFoldersOpenClose = 1

" enable pattern matching glyphs on folder/directory (enabled by default with 1)
let g:DevIconsEnableFolderPatternMatching = 1

" enable file extension pattern matching glyphs on folder/directory (disabled by default with 0)
let g:DevIconsEnableFolderExtensionPatternMatching = 0


"=====================================================
"" SnipMate settings
"=====================================================
let g:snippets_dir='~/.vim/vim-snippets/snippets'

"=====================================================
"" Indent Guides Settings 
"=====================================================
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.
"=====================================================
"" Python settings
"=====================================================

" highlight 'long' lines (>= 80 symbols) in python files
"augroup vimrc_autocmds
"    autocmd!
"    autocmd FileType python,c,cpp highlight Excess ctermbg=DarkGrey guibg=Black
"    autocmd FileType python,c,cpp match Excess /\%81v.*/
"    autocmd FileType python,c,cpp set nowrap
"    autocmd FileType python,c,cpp set colorcolumn=80
"    autocmd FileType text call pencil#init()
"augroup END


python3 << EOF
import vim
import git
def is_git_repo():
	try:
		_ = git.Repo('.', search_parent_directories=True).git_dir
		return "1"
	except:
		return "0"
vim.command("let g:pymode_rope = " + is_git_repo())
EOF

"=====================================================
"" C/C++ settings
"=====================================================
let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-5.0/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/lib/llvm-5.0/lib/clang'
let g:deoplete#sources#clang#std = {'c': 'c11', 'cpp': 'c++14'}



""  Copy to clipboard
" vnoremap  <leader>y  "*y
" nnoremap  <leader>Y  "*yg_
" nnoremap  <leader>y  "*y
"" 
"" Paste from clipboard
" nnoremap <leader>p "*p
" nnoremap <leader>P "*P
" vnoremap <leader>p "*p
" vnoremap <leader>P "*P


"function! ClipboardYank()
"  call system('xclip -i -selection clipboard', @@)
"endfunction
"function! ClipboardPaste()
"  let @@ = system('xclip -o -selection clipboard')
"endfunction
"
"vnoremap <silent> y y:call ClipboardYank()<cr>
"vnoremap <silent> d d:call ClipboardYank()<cr>
"nnoremap <silent> p :call ClipboardPaste()<cr>p
"onoremap <silent> y y:call ClipboardYank()<cr>
"onoremap <silent> d d:call ClipboardYank()<cr>


map <silent> <M-h> <C-w><
map <silent> <M-j> <C-W>-
map <silent> <M-k> <C-W>+
map <silent> <M-l> <C-w>>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

:set mouse=a
map <ScrollWheelUp> <C-Y>
map <ScrollWheelDown> <C-E>

"map <F8> :hi Normal guibg=NONE ctermbg=NONE <CR>


"let t:is_transparent = 0
"function! Toggle_transparent()
"    if t:is_transparent == 0
"        hi Normal guibg=NONE ctermbg=NONE
"        let t:is_transparent = 1
"    else
"        set background=light
"        let t:is_tranparent = 0
"    endif
"endfunction
"nnoremap <F8> : call Toggle_transparent()<CR>
