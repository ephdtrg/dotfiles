#!/bin/bash

echo 1 - desktop, 2 - dual horizontal, 3 - dual vertical, 4 - laptop, 5 - extend
read -p 'var: ' setupvar

if [ $setupvar = 1 ]
then
    xrandr --output VGA-1 --auto --primary --output LVDS-1 --off
    bspc monitor LVDS-1 -r
    bspc monitor -d I II III IV V VI VII VIII IX
    bspc config top_padding 21
elif [ $setupvar = 2 ]
then  
    xrandr --output VGA-1 --auto --right-of LVDS-1
    for i in I II III IV V; do bspc desktop $i --to-monitor VGA-1; done
    bspc desktop Desktop --remove
    bspc config -m VGA-1 bottom_padding 0
elif [ $setupvar = 3 ]
then
    xrandr --output VGA-1 --auto  --rotate left --right-of LVDS-1
    for i in V VI VII; do bspc desktop $i --to-monitor VGA-1; done
    bspc desktop Desktop --remove
elif [ $setupvar = 4 ]
then
    for i in I II III IV V; do bspc desktop $i --to-monitor LVDS-1; done
    xrandr --output LVDS-1 --auto --primary --output VGA-1 --off
    bspc monitor VGA-1 -r
    bspc monitor LVDS-1 -o I II III IV V VI VII VIII IX X
    bspc monitor LVDS-1 -d I II III IV V VI VII VIII IX X
elif [ $setupvar = 5 ]
then
    xrandr --output VGA-1 --auto --right-of LVDS-1
    for i in VI VII VIII IX; do bspc desktop $i --to-monitor VGA-1; done
    bspc desktop Desktop -remove
    bspc config -m VGA-1 bottom_padding 0
elif [ $setupvar = 6 ]
then
    for i in VI VII VIII IX; do bspc desktop $i --to-monitor LVDS-1; done
    bspc monitor VGA-1 -r
    bspc desktop Desktop -remove
    bspc monitor LVDS-1 -o I II III IV V VI VII VIII IX X
    bspc monitor LVDS-1 -d I II III IV V VI VII VIII IX X
fi

echo success
