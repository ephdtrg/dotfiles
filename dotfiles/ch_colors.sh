#!/bin/bash

config_color=$1

if [ $config_color = "black" ]; then
    ln -snf ~/.emacs.d/multiconfig/black.el ~/.emacs.d/init.el
    ln -snf ~/.config/nvim/multiconfig/black.vim ~/.config/nvim/init.vim
    echo "done default vim and emacs"
elif [ $config_color = "zenburn" ]; then
    ln -snf ~/.emacs.d/multiconfig/zenburn.el ~/.emacs.d/init.el
    ln -snf ~/.config/nvim/multiconfig/zenburn.vim ~/.config/nvim/init.vim
    echo "done zenburn theme for vim/emacs"
else
    echo "failed, invalid args"
fi
