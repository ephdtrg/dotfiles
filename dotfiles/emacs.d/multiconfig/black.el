(load "/home/euphemia/.emacs.d/el-get/benchmark-init/benchmark-init.el"
      'no-error nil 'no-suffix)

; invoke el-get
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(el-get 'sync)

(setq
 my:el-get-packages
 '(el-get
    company-mode
    color-theme
    color-theme-zenburn))

(add-hook 'solidity-mode-hook
   (lambda ()
     (set (make-local-variable 'company-backends)
          (append '((company-solidity company-capf company-dabbrev-code))
                  company-backends))))

(setq auto-mode-alist (append '(("\\.sol\\'" . solidity-mode)) auto-mode-alist))
(setq auto-mode-alist (append '(("\\.js\\'" . js2-mode)) auto-mode-alist))

(add-hook 'after-init-hook 'global-company-mode)
;(add-hook 'after-init-hook (lambda () (load-theme 'zenburn t)))
(add-hook 'after-init-hook (lambda () (load-theme 'dracula t)))
(add-hook 'after-init-hook 'nyan-mode)


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("aaffceb9b0f539b6ad6becb8e96a04f2140c8faa1de8039a343a4f1e009174fb" "190a9882bef28d7e944aa610aa68fe1ee34ecea6127239178c7ac848754992df" "01e067188b0b53325fc0a1c6e06643d7e52bc16b6653de2926a480861ad5aa78" "c79c2eadd3721e92e42d2fefc756eef8c7d248f9edefd57c4887fbf68f0a17af" "256a381a0471ad344e1ed33470e4c28b35fb4489a67eb821181e35f080083c36" "158013ec40a6e2844dbda340dbabda6e179a53e0aea04a4d383d69c329fba6e6" "b563a87aa29096e0b2e38889f7a5e3babde9982262181b65de9ce8b78e9324d5" "3fa07dd06f4aff80df2d820084db9ecbc007541ce7f15474f1d956c846a3238f" "e30f381d0e460e5b643118bcd10995e1ba3161a3d45411ef8dfe34879c9ae333" "da538070dddb68d64ef6743271a26efd47fbc17b52cc6526d932b9793f92b718" "003a9aa9e4acb50001a006cfde61a6c3012d373c4763b48ceb9d523ceba66829" "9b1c580339183a8661a84f5864a6c363260c80136bd20ac9f00d7e1d662e936a" "af717ca36fe8b44909c984669ee0de8dd8c43df656be67a50a1cf89ee41bde9a" "d21135150e22e58f8c656ec04530872831baebf5a1c3688030d119c114233c24" "c616e584f7268aa3b63d08045a912b50863a34e7ea83e35fcab8537b75741956" "cf284fac2a56d242ace50b6d2c438fcc6b4090137f1631e32bedf19495124600" "228c0559991fb3af427a6fa4f3a3ad51f905e20f481c697c6ca978c5683ebf43" "8d5f22f7dfd3b2e4fc2f2da46ee71065a9474d0ac726b98f647bc3c7e39f2819" "66aea5b7326cf4117d63c6694822deeca10a03b98135aaaddb40af99430ea237" "a94f1a015878c5f00afab321e4fef124b2fc3b823c8ddd89d360d710fc2bddfc" "0cd56f8cd78d12fc6ead32915e1c4963ba2039890700458c13e12038ec40f6f5" "de0b7245463d92cba3362ec9fe0142f54d2bf929f971a8cdf33c0bf995250bcf" "3eb93cd9a0da0f3e86b5d932ac0e3b5f0f50de7a0b805d4eb1f67782e9eb67a4" "251348dcb797a6ea63bbfe3be4951728e085ac08eee83def071e4d2e3211acc3" "d61f6c49e5db58533d4543e33203fd1c41a316eddb0b18a44e0ce428da86ef98" "721bb3cb432bb6be7c58be27d583814e9c56806c06b4077797074b009f322509" "1b27e3b3fce73b72725f3f7f040fd03081b576b1ce8bbdfcb0212920aec190ad" "946e871c780b159c4bb9f580537e5d2f7dba1411143194447604ecbaf01bd90c" "73a13a70fd111a6cd47f3d4be2260b1e4b717dbf635a9caee6442c949fad41cd" "b59d7adea7873d58160d368d42828e7ac670340f11f36f67fa8071dbf957236a" default)))
 '(indent-tabs-mode nil)
 '(linum-format "%3d ")
 '(package-selected-packages
   (quote
    (dracula-theme org-link-minor-mode zenburn-theme seq let-alist)))
 '(smooth-scroll-mode t)
 '(smooth-scroll/hscroll-step-size 1)
 '(smooth-scroll/vscroll-step-size 1))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum ((t (:background "#262626" :foreground "#A8A8A8"))))
 '(whitespace-tab ((t (:background "red")))))

; Highlight tabs and trailing whitespace everywhere
(setq whitespace-style '(face trailing tabs))

(global-whitespace-mode)

(require 'xclip)
(xclip-mode 1)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default c-basic-offset 4)
(setq js-indent-level 4)

(add-hook 'js2-mode-hook #'js2-imenu-extras-mode)

(add-hook 'js2-mode-hook #'js2-refactor-mode)
(js2r-add-keybindings-with-prefix "C-c C-r")
(define-key js2-mode-map (kbd "C-k") #'js2r-kill)

;; js-mode (which js2 is based on) binds "M-." which conflicts with xref, so
;; unbind it.
(define-key js-mode-map (kbd "M-.") nil)

(add-hook 'js2-mode-hook (lambda ()
  (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))

(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)

;; =============
;; irony-mode
;; =============

(defun my-irony-mode-on ()
  ;; avoid enabling irony-mode in modes that inherits c-mode, e.g: php-mode
  (when (member major-mode irony-supported-major-modes)
    (irony-mode 1)))

(add-hook 'c++-mode-hook 'my-irony-mode-on)
(add-hook 'c-mode-hook 'my-irony-mode-on)
;; =============
;; company mode
;; =============
(add-hook 'c++-mode-hook 'company-mode)
(add-hook 'c-mode-hook 'company-mode)
;; replace the `completion-at-point' and `complete-symbol' bindings in
;; irony-mode's buffers by irony-mode's function
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))
;; (optional) adds CC special commands to `company-begin-commands' in order to
;; trigger completion at interesting places, such as after scope operator
;;     std::|
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)
;; =============
;; flycheck-mode
;; =============
(add-hook 'c++-mode-hook 'flycheck-mode)
(add-hook 'c-mode-hook 'flycheck-mode)
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))
;; =============
;; eldoc-mode
;; =============
(add-hook 'irony-mode-hook 'irony-eldoc)
;; ==========================================
;; (optional) bind TAB for indent-or-complete
;; ==========================================
(defun irony--check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "->") t nil)))))
(defun irony--indent-or-complete ()
  "Indent or Complete"
  (interactive)
  (cond ((and (not (use-region-p))
              (irony--check-expansion))
         (message "complete")
         (company-complete-common))
        (t
         (message "indent")
         (call-interactively 'c-indent-line-or-region))))
(defun irony-mode-keys ()
  "Modify keymaps used by `irony-mode'."
  (local-set-key (kbd "TAB") 'irony--indent-or-complete)
  (local-set-key [tab] 'irony--indent-or-complete))
(add-hook 'c-mode-common-hook 'irony-mode-keys)
     ```
(set-face-attribute 'fringe nil :background "red")
(setq-default left-fringe-width  10)
(setq linum-format "%3d ")
(add-hook 'after-init-hook 'global-linum-mode)

(setq indent-tabs-mode nil)


(face-spec-set 'fringe
  '((((class color) (background light))
     :background "red")
    (((class color) (background dark))
     :background "yellow")
    (t
     :background "cyan")))

(require 'powerline)
(powerline-default-theme)
(require 'airline-themes)
;(load-theme 'airline-ubaryd)
(load-theme 'airline-dark)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time   
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time

;; use customized linum-format: add a addition space after the line number
;(setq linum-format (lambda (line) (propertize (format (let ((w (length (number-to-string (count-lines (point-min) (point-max)))))) (concat "%"
;(number-to-string w) "d ")) line) 'face 'linum)))
;(setq linum-format "%4d ")
;(require 'hl-line)
;
;(defface my-linum-hl
;  `((t :inherit linum :background ,(face-background 'hl-line nil t)))
;  "Face for the current line number."
;  :group 'linum)
;
;(defvar my-linum-format-string "%3d")
;
;(add-hook 'linum-before-numbering-hook 'my-linum-get-format-string)
;
;(defun my-linum-get-format-string ()
;  (let* ((width (1+ (length (number-to-string
;                             (count-lines (point-min) (point-max))))))
;         (format (concat "%" (number-to-string width) "d")))
;    (setq my-linum-format-string format)))
;
;(defvar my-linum-current-line-number 0)
;
;(setq linum-format 'my-linum-format)
;
;(defun my-linum-format (line-number)
;  (propertize (format my-linum-format-string line-number) 'face
;              (if (eq line-number my-linum-current-line-number)
;                  'my-linum-hl
;                'linum)))
;
;(defadvice linum-update (around my-linum-update)
;  (let ((my-linum-current-line-number (line-number-at-pos)))
;    ad-do-it))
;(ad-activate 'linum-update)

(use-package solidity-mode
    :ensure t
    :config
    (setq solidity-solc-path "/home/euphemia/.npm-global/bin/solcjs")
    (setq solidity-solium-path "/home/euphemia/.npm-global/bin/solium")
    ;(setq flycheck-solidity-solium-soliumrcfile "/home/euphemia/EPH.w/eth/.soliumrc.json")
    (setq solidity-flycheck-solc-checker-active t)
    (setq solidity-flycheck-solium-checker-active t)
    (setq flycheck-solidity-solc-addstd-contracts t)
    (setq solidity-flycheck-chaining-error-level t)
    :bind ("C-c C-g" . 'solidity-estimate-gas-at-point))


(use-package irony
  :hook (((c++-mode c-mode objc-mode) . irony-mode-on-maybe)
         (irony-mode . irony-cdb-autosetup-compile-options))
  :config
  (defun irony-mode-on-maybe ()
    ;; avoid enabling irony-mode in modes that inherits c-mode, e.g: solidity-mode
    (when (member major-mode irony-supported-major-modes)
      (irony-mode 1))))


