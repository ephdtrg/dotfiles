[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Dracula
Font=Source Code Pro Semibold,8,-1,0,63,0,0,0,0,0,Regular
UseFontLineChararacters=true

[General]
Command=/usr/bin/zsh
Name=Profile 5
Parent=FALLBACK/

[Scrolling]
HistorySize=4000
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
